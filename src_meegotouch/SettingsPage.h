/*
    This file is part of mtermite

    Copyright (C) 2012 by Ruslan Mstoi <ruslan.mstoi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/

#ifndef _SETTINGS_PAGE_
#define _SETTINGS_PAGE_

#include <MApplicationPage>

class MTermWidget;
class MComboBox;
class MContainer;

class SettingsPage : public MApplicationPage
{
    Q_OBJECT

public:
    SettingsPage(QGraphicsItem* parent = 0);
    ~SettingsPage();
    void createContent();

protected slots:
    void readVolatileSettings();
    void enableSipWidgets(bool disable);

private:
    MContainer* createUiContainer();
    MContainer* createSipContainer();

    MTermWidget *m_parentTermWidget;
    MComboBox *m_toolbarsComboBox;
    QList<QGraphicsItem *> sipItems; // items to disable when sip is not in use
};

#endif
