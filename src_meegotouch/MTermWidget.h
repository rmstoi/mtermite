/*
    This file is part of mtermite

    Copyright (C) 2012 by Ruslan Mstoi <ruslan.mstoi@gmail.com>

    Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).

    Contact: Ruslan Mstoi <ruslan.mstoi@nokia.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/

#ifndef _MTERM_WIDGET
#define _MTERM_WIDGET

#include "qgraphicstermwidget.h"

const QString APP_NAME = "MTermite";
const QString APP_NAME_SETTINGS = "mtermite";

class MPannableViewport;
class MButton;
class MComboBox;
class MAction;
class MTerminalDisplay;

enum ORIENTATION {
    ORIENTATION_AUTOMATIC,
    ORIENTATION_PORTRAIT,
    ORIENTATION_LANDSCAPE,

    ORIENTATION_FIRST = ORIENTATION_AUTOMATIC,
    ORIENTATION_LAST = ORIENTATION_LANDSCAPE
};

const QString ORIENTATION_NAME[] = {
    "Automatic",
    "Portrait",
    "Landscape"
};

/**
 * MeeGo touch terminal widget.
 *
 * This widget adds meegotouch based input method, copy/paste, panning,
 * support to the terminal widget.
 */
class MTermWidget : public QGraphicsTermWidget
{
    Q_OBJECT
public:
    MTermWidget(int startnow = 1, //start shell programm immediatelly
                QGraphicsWidget *parent = 0);

    ~MTermWidget();

    void setColorScheme(int scheme);
    COLOR_SCHEME colorScheme() const { return m_colorScheme; };
    void startShellProgram();
    ORIENTATION orientation() const { return m_orientation; }
    MTerminalDisplay* display() { return m_display; };
    bool isFullScreen() const;
    bool sipEnabled() const { return m_sipEnabled; }
    bool sipTranslucent() const { return m_sipTranslucent; }
    bool getShowBanners() const { return m_showBanners; }
    bool blinkingCursor() const;
    int keyboardCursorShape() const;
    bool expandIfSipHides() const { return m_expandIfSipHides; }

public slots:
    void toggleFullScreenMode(bool fullScreen);
    void toggleSelectionMode(bool selection);
    void toggleSelectionModeWithBanner(bool selection);
    void createNewWindow() const;
    void showHelp();
    void changeOrientation(int index);
    void changeColorScheme(int scheme);
    void clearHistory();
    void clearHistoryAndReset();
    void showSettingsPage();
    void enableSoftwareInputPanel(bool enable);
    void setSipTranslucent(bool translucent);
    void setShowBanners(bool show) { m_showBanners = show; }
    void setBlinkingCursor(bool blink);
    void setKeyboardCursorShape(int shape);
    void setExpandIfSipHides(bool expand) { m_expandIfSipHides = expand; }

protected:
    TerminalDisplay* createTerminalDisplay(Session *session);
    void resizeEvent(QGraphicsSceneResizeEvent *event);
    bool event(QEvent *event);

private:
    void showBanner(const QString &title) const;
    bool gestureEvent(QGestureEvent *event);
    void pinchTriggered(QPinchGesture*);
    void swipeTriggered(QSwipeGesture*);

    void construct(int startnow);

    void readSettings();
    void writeSettings() const;

    void setupMenu();
    void enableGestures();
    void disableGestures();

    MPannableViewport *m_viewport;

    /* Last scroll of display history set by this widget due to viewport
     panning. It is used to avoid the rounding error when converting from lines
     into the viewport position.
     
     Details: viewportPositionChanged is called by viewport whenever position
     changes and it can set position to a number which is not a multiplier of
     PANM. But since scroll can be set only in integer lines, division will
     result in a decimal number with a fraction, which will be rounded
     off. Call to viewportPositionChanged will eventually result in a call
     updateViewport will be called with the same currentLine.  This in turn
     will set position to a number that is a full integer multiplier of
     PANM. So this will result in viewport position bouncing back and forth: a
     very weird panning artifacts. */
    int m_lastSetCurrentLine;

    COLOR_SCHEME m_colorScheme; // used color scheme

    // with default theme navigation bar is hidden by the VKB so custom
    // button is used to invoke the menu
    MButton *m_menuButton;
    MButton *m_selectionButton;

    MTerminalDisplay* m_display; // casted m_terminalDisplay

    ORIENTATION m_orientation;
    bool m_sipEnabled;
    bool m_sipTranslucent;
    bool m_showBanners;

    // if set, terminal display will be resized to take more space if sip hides
    // this setting is inactive if sip translucency is enabled, because in that
    // case display is always of full size
    bool m_expandIfSipHides;

private slots:
    void updateViewport(int currentLine, int maxLines);
    void viewportPositionChanged(const QPointF &position);
    void onInputMethodAreaChanged(const QRect &);
    void displayFontChanged() const;
    void openLink(const QString& link) const;
};

#endif
