/*
    This file is part of mtermite

    Copyright (C) 2012 by Ruslan Mstoi <ruslan.mstoi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301  USA.
*/

#include <QGraphicsLinearLayout>

#include <MContainer>
#include <MButton>
#include <MComboBox>
#include <MLabel>
#include <MPannableViewport>

#include "SettingsPage.h"
#include "MTermWidget.h"
#include "MTerminalDisplay.h"

/**
 * A switch button with a label in front of it.
 *
 * layout is the owner of the label and button, so make sure it is destroyed.
 */
struct LabeledSwitch
{
    LabeledSwitch(const QString &text,
                  bool checked,
                  const QObject *receiver = 0,
                  const char *method = 0);

    MLabel *label;
    MButton *button;
    QGraphicsLinearLayout *layout;

};

/**
 * Creates a switch button with a label in front of it.
 *
 * Slot 'method' of the 'receiver' will be connected to the toggled signal of
 * the switch.
 */
LabeledSwitch::LabeledSwitch(const QString &text,
                             bool checked,
                             const QObject *receiver,
                             const char *method)
{
    button = new MButton();
    button->setViewType(MButton::switchType);
    button->setCheckable(true);
    button->setChecked(checked);
    if (receiver && method) {
        QObject::connect(button, SIGNAL(toggled(bool)), receiver, method);
    }

    label = new MLabel(text);
    label->setTextElide(true);
    label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

    layout = new QGraphicsLinearLayout(Qt::Horizontal);
    layout->addItem(label);
    layout->addItem(button);
    layout->setAlignment(button, Qt::AlignCenter);
}

SettingsPage::SettingsPage(QGraphicsItem* parent):
    MApplicationPage(parent),
    m_parentTermWidget(0),
    m_toolbarsComboBox(0)
{
    m_parentTermWidget = dynamic_cast<MTermWidget*>(parent);

    connect(this, SIGNAL(appearing()), SLOT(readVolatileSettings()));
}

SettingsPage::~SettingsPage()
{
}

MContainer* SettingsPage::createUiContainer()
{
    MContainer *uiContainer = new MContainer("User Interface");
    QGraphicsLinearLayout *uiLayout =
        new QGraphicsLinearLayout(Qt::Vertical,
                                  uiContainer->centralWidget());

    uiLayout->addItem(
        LabeledSwitch(
            "Full Screen", m_parentTermWidget->isFullScreen(),
            m_parentTermWidget, SLOT(toggleFullScreenMode(bool))).layout);

    MComboBox *orientationComboBox = new MComboBox(this);
    for (int i = ORIENTATION_FIRST; i <= ORIENTATION_LAST; ++i)
        orientationComboBox->addItem(ORIENTATION_NAME[i]);
    orientationComboBox->setIconVisible(false);
    orientationComboBox->setTitle("Orientation");
    orientationComboBox->setCurrentIndex(m_parentTermWidget->orientation());
    connect(orientationComboBox, SIGNAL(currentIndexChanged(int)),
            m_parentTermWidget, SLOT(changeOrientation(int)));
    uiLayout->addItem(orientationComboBox);

    MComboBox *colorSchemeComboBox = new MComboBox(this);
    for (int i = COLOR_SCHEME_FIRST; i <= COLOR_SCHEME_LAST; ++i)
        colorSchemeComboBox->addItem(COLOR_SCHEME_NAME[i]);
    colorSchemeComboBox->setIconVisible(false);
    colorSchemeComboBox->setTitle("Color Scheme");
    colorSchemeComboBox->setCurrentIndex(m_parentTermWidget->colorScheme());
    connect(colorSchemeComboBox, SIGNAL(currentIndexChanged(int)),
            m_parentTermWidget, SLOT(changeColorScheme(int)));
    uiLayout->addItem(colorSchemeComboBox);

    uiLayout->addItem(
        LabeledSwitch(
            "Blinking Cursor", m_parentTermWidget->blinkingCursor(),
            m_parentTermWidget, SLOT(setBlinkingCursor(bool))).layout);

    const QString KeyboardCursorShapeName[] = {
        "Block",
        "Underline",
        "I-Beam"
    };

    MComboBox *cursorShapeComboBox = new MComboBox(this);
    for (int i = TerminalDisplay::FirstCursor; i <= TerminalDisplay::LastCursor; ++i)
        cursorShapeComboBox->addItem(KeyboardCursorShapeName[i]);
    cursorShapeComboBox->setIconVisible(false);
    cursorShapeComboBox->setTitle("Cursor Shape");
    cursorShapeComboBox->setCurrentIndex(
        m_parentTermWidget->keyboardCursorShape());
    connect(cursorShapeComboBox, SIGNAL(currentIndexChanged(int)),
            m_parentTermWidget, SLOT(setKeyboardCursorShape(int)));
    uiLayout->addItem(cursorShapeComboBox);

    uiLayout->addItem(
        LabeledSwitch(
            "Show Help Banners", m_parentTermWidget->getShowBanners(),
            m_parentTermWidget, SLOT(setShowBanners(bool))).layout);

    return uiContainer;
}

MContainer* SettingsPage::createSipContainer()
{
    MContainer *sipContainer = new MContainer("Virtual Keyboard");
    QGraphicsLinearLayout *sipLayout =
        new QGraphicsLinearLayout(Qt::Vertical,
                                  sipContainer->centralWidget());

    LabeledSwitch sipEnabledSwitch =
        LabeledSwitch("Enabled", m_parentTermWidget->sipEnabled());
    connect(sipEnabledSwitch.button, SIGNAL(toggled(bool)),
            m_parentTermWidget, SLOT(enableSoftwareInputPanel(bool)));
    connect(sipEnabledSwitch.button, SIGNAL(toggled(bool)),
            SLOT(enableSipWidgets(bool)));
    sipLayout->addItem(sipEnabledSwitch.layout);

    MTerminalDisplay* display = m_parentTermWidget->display();

    m_toolbarsComboBox = new MComboBox(this);
    foreach(MTerminalDisplay::Toolbar* toolbar, display->toolbars())
        m_toolbarsComboBox->addItem(toolbar->name);
    m_toolbarsComboBox->setIconVisible(false);
    m_toolbarsComboBox->setTitle("Toolbar");
    // initial currentIndex will be set in readVolatileSettings
    connect(m_toolbarsComboBox, SIGNAL(currentIndexChanged(int)),
            display, SLOT(setActiveToolbarIndex(int)));
    sipLayout->addItem(m_toolbarsComboBox);

    LabeledSwitch sipTranslucentSwitch =
        LabeledSwitch("Translucent", m_parentTermWidget->sipTranslucent());
    connect(sipTranslucentSwitch.button, SIGNAL(toggled(bool)),
            m_parentTermWidget, SLOT(setSipTranslucent(bool)));
    sipLayout->addItem(sipTranslucentSwitch.layout);

    LabeledSwitch expandIfSipHidesSwitch =
        LabeledSwitch("Expand Terminal If VKB Hides",
                      m_parentTermWidget->expandIfSipHides());
    connect(expandIfSipHidesSwitch.button, SIGNAL(toggled(bool)),
            m_parentTermWidget, SLOT(setExpandIfSipHides(bool)));
    sipLayout->addItem(expandIfSipHidesSwitch.layout);

    sipItems << m_toolbarsComboBox << sipTranslucentSwitch.button <<
        expandIfSipHidesSwitch.button;

    enableSipWidgets(m_parentTermWidget->sipEnabled());  // set initial state

    return sipContainer;
}

/**
 * "Framework calls this function only once". So all non-volatile settings are
 * initialized here.
 */
void SettingsPage::createContent()
{
    MApplicationPage::createContent();

    MContainer* uiContainer = createUiContainer();
    MContainer* sipContainer = createSipContainer();

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout(Qt::Vertical,
                                                              centralWidget());
    layout->addItem(uiContainer);
    layout->addItem(sipContainer);
}

void SettingsPage::enableSipWidgets(bool enable)
{
    foreach (QGraphicsItem *item, sipItems) {
        item->setEnabled(enable);
    }
}

/**
 * Updates the widgets of the volatile settings with the current values
 * whenever this page is shown.
 *
 * The volatile settings are settings that can be changed by other means in
 * addition to this settings page. For example, some could be changed by
 * gestures too.
 */
void SettingsPage::readVolatileSettings()
{
    MTerminalDisplay* display = m_parentTermWidget->display();
    m_toolbarsComboBox->setCurrentIndex(display->activeToolbarIndex());
}
