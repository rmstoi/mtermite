Improved terminal emulator application for Nokia N9.

This project was started because upgrades to meego-terminal were not possible. Hence mtermite is based on meego-terminal, but has more features and bug fixes. mtermite application was distributed in ovi store.