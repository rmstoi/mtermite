TEMPLATE        = app
DESTDIR         = ..

CONFIG          += qt debug_and_release warn_on build_all

QT += core gui

MOC_DIR         = ../.moc
LIBS            += -L..

CONFIG(debug, debug|release) {
    OBJECTS_DIR = ../.objs_d
    TARGET      = graphicsview-terminal_d
    LIBS        += -lqgraphicstermwidget_d
    PRE_TARGETDEPS += ../libqgraphicstermwidget_d.a
} else {
    OBJECTS_DIR = ../.objs
    TARGET      = graphicsview-terminal
    LIBS        += -lqgraphicstermwidget
    PRE_TARGETDEPS += ../libqgraphicstermwidget.a
}

SOURCES         = main_graphicsview.cpp 

INCLUDEPATH     = ../lib

#LIBS           += -L.. -lqgraphicstermwidget

